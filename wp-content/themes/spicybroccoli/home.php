<?php get_header(); ?>
    <section id="page"  role="main">
        <div class="container">
        	<?php while ( have_posts() ) : the_post(); ?>
	        	<h1><?php echo $post->post_title; ?></h1>
	            <?php 
	            $content = trim(stripslashes($post->post_content));
	            $html = apply_filters("the_content", $content);
	            echo $html; ?>
            <?php endwhile; ?>
        </div><!-- #container -->
    </section><!-- #main -->

<?php get_footer(); ?>