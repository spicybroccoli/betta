<?php
/**
 * The Header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */
?><!DOCTYPE html>
<?php
/**
 * The Header template for our theme
 */
?><html <?php language_attributes(); ?>>
<?php echo '<!-- ' . basename( get_page_template() ) . ' -->'; ?>
<head>
<title><?php
	/*
	 * Print the <title> tag based on what is being viewed.
	 */
	global $page, $paged;

	wp_title( '|', true, 'right' );

	// Add the blog name.
	bloginfo( 'name' );

	// Add the blog description for the home/front page.
	$site_description = get_bloginfo( 'description', 'display' );
	if ( $site_description && ( is_home() || is_front_page() ) )
		echo " | $site_description";

	// Add a page number if necessary:
	if ( $paged >= 2 || $page >= 2 )
		echo ' | ' . sprintf( __( 'Page %s', 'twentyten' ), max( $paged, $page ) );

	?></title>
<link rel="profile" href="http://gmpg.org/xfn/11" />
<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo( 'stylesheet_url' ); ?>" />
<link rel="stylesheet" type="text/css" media="all" href="<?php echo get_stylesheet_directory_uri(); ?>/style-responsive.css" />
<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/modernizr.js"></script>
<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="mp-pusher" class="mp-pusher">
<div class="main-menu">
			  <?php
                    wp_nav_menu( 
                        array( 
                           'menu' => 'primary',
                            'container'  => 'false'
                        ) 
                    ); 
                ?>	
          </div>
    <nav class="mp-menu" id="mp-menu">
		<div class="mp-level">    	
	        <?php
	           	wp_nav_menu( 
	            	array( 
	            		'menu' => 'primary',
	            		'container'  => 'false',
	            		'container_id' => 'mp-menu',
	            		'menu_class' => 'mp-level',
	            		'container_class' => 'mp-menu',
	            		'menu_class' => '',  
	            		'walker' => new SBM_Responsive_Nav()
	            	) 
	            ); 
	        ?>	
                    
        </div>
    </nav>
	<div id="wrapper" class="hfeed">
	<section id="header">
    <div class="container">
      <div class="row">
        <div class="logo col-size-1 col-sm-2">
<a href="<?php echo site_url(); ?> ">
<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/betta-storage-logo.png" class="img-responsive" alt="<?php echo bloginfo( 'name' ); ?>"/>
</a> 
</div>
        <div class="col-size-3 header-right">
            <div class="phone-header"><a href="<?php echo get_permalink( 32 ); ?>" class="button button-header">Get a quote</a><span>1800 800 042</span></div>
            <div class="social-media"> <a href="#" class="facebook"></a> <a href="#" class="twitter"></a> <a href="#" class="gplus"></a> </div>
          </div>
          <div class="col-size-3 header-right">
          <div class="main-menu">
			  <?php
                    wp_nav_menu( 
                        array( 
                           'menu' => 'primary',
                            'container'  => 'false'
                        ) 
                    ); 
                ?>	
          </div>
          </div>
    <div id="menu-trigger" class="col-sm-2">
		<a href="#" id="trigger" class="menu-trigger"></a>
	</div>
      </div>
    </div>
  </section>
  <?php if(is_front_page()){ ?>
  <section id="slider"><?php 
    echo do_shortcode("[metaslider id=15]"); 
?></section>
<?php } ?>
<?php 
if(!is_front_page()) {
			//GET THE FIXED IG HEADER
				get_template_part('content', 'img-header');
			//GET THE LINKS BAR
				get_template_part('content', 'links');
			
			if(!is_page()){
			//GET THE LINKS BAR
				get_template_part('content', 'bread');
			}
		echo "<article>";
}
?>