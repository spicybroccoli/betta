<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */

get_header(); ?>

<section id="services">
  <div class="container">
    <div class="row">
      <div class="col-size-2">
      <div class="row">
      <?php get_template_part('content', 'bread'); ?>
      </div>
        <h1><?php echo $post->post_title; ?></h1>
        <?php 
            $content = trim(stripslashes($post->post_content));
            $html = apply_filters("the_content", $content);
            echo $html; ?>
      </div>
      <div class="col-size-2">
        <div class="find-your-container">
        	<?php if(is_page( 111 )){ //SELL CONTAINER PAGE ?>
			<h2>Sell Your Container</h2>
            		<?php echo do_shortcode('[contact-form-7 id="114" title="Container Sell Enquiry"]'); ?>
		<?php }else{ ?>
			<h2>Find Your Container</h2>
            		<?php echo do_shortcode('[contact-form-7 id="80" title="Container Enquiry"]'); ?>
		<?php } ?>
        </div>
      </div>
    </div>
  </div>
  <!-- #container --> 
</section>
<!-- #main -->

<?php get_footer(); ?>