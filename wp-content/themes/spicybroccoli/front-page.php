<?php
/**
 * The main template file.
 **/

get_header(); ?>

<section id="containers">
  <div class="container">
    <div class="green-container">
      <div class="key"> </div>
      <p><a href="<?php echo site_url(); ?>/products/hire-a-container/">Hire a 
        Container</a></p>
    </div>
    <div class="green-container-separator left"> </div>
    <div class="green-container">
      <div class="coin"> </div>
      <p><a href="<?php echo site_url(); ?>/products/buy-a-container/">Buy a
        Container</a></p>
    </div>
    <div class="green-container-separator right"> </div>
    <div class="green-container">
      <div class="truck"> </div>
      <p><a href="<?php echo site_url(); ?>/products/delivery-services/">Delivery
        Services</a></p>
    </div>
  </div>
</section>
<?php
//GET THE LINKS BAR
get_template_part( 'content', 'links' );
?>
<section id="aboutus">
  <div class="container">
    <div class="row">
      <div class="col-size-2">
        <div class="white-box">
          <?php $post_2 = get_post( 2 ); ?> 
          <h2><?php echo $post_2->post_title; ?></h2>
          <p><?php echo get_post_meta( 2, 'intro_text', true ); ?></p>
          <a class="button button-grey-green" href="<?php echo get_permalink( 2 ); ?>">Read More</a> </div>
      </div>
      <div class="col-size-2">
        <div class="white-box">
          <?php $post_13 = get_post( 13 ); ?> 
          <h2><?php echo $post_13->post_title; ?></h2>
          <p><?php echo get_post_meta( 13, 'intro_text', true ); ?></p>
          <a class="button button-grey-green" href="<?php echo get_permalink( 13 ); ?>">Read More</a> </div>
      </div>
    </div>
  </div>
</section>
<section id="get-quote">
  <div class="container">
    <div class="row">
      <div class="col-size-2">
        <p>Contact Us for a quote</p>
      </div>
      <div class="col-size-2"> <a class="button button-yellow" href="<?php echo get_permalink( 32 ); ?>">Get a quote</a> </div>
    </div>
  </div>
</section>
<style>
.green-container p a {
    font-family: "Playfair Display","Open Sans",serif;
    text-align: center;
    color: #FDE44B;
    text-shadow: 0.08em 0.08em #4C4C4C;}
</style>

<?php get_footer(); ?>
