<section id="links">
    <div class="container">
        <div class="links-menu">
            <p><span id="length"  class="Tselected">Length</span> or <strong id="type">Type</strong></p>

            <ul  class="product-length">
                <?php	//*/Show all tags
                $args = array(	'echo' 		=> 1, 'format' => 'array');
                $wptc = wp_tag_cloud( $args );
                //$wptc = explode('</a>', $wptc);
                foreach( $wptc as $wpt ) echo "<li>" . $wpt . "</li>";  //*/
                ?>
            </ul>
            <ul class="product-type" style="display: none;">
                <?php
                //* SHOW ALL CATEGORIES
                $args = array(
                    'child_of' => 5,
                    'title_li' => '',
                    'hide_empty' => 0

                );
                wp_list_categories( $args ); //*/
                ?>
            </ul>

        </div>
</section>