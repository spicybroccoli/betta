	<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */

get_header();
/* GET ALL CATEGORY FROM POSTOB*/
$args = array(
	'orderby' => 'slug', 
	'order' => 'ASC'
);
//$category = wp_get_post_categories( get_the_ID(), $args );
$category = wp_get_post_terms( get_the_ID(), 'category', $args );
//var_dump($category );
foreach($category as $key => $value) {
//var_dump($value);
//var_dump($key);
  if($value->term_id == 5) {   //remove parent category
    unset($category[$key]);
	$category = array_values($category);
  }
}
 ?>
  <section id="product">
    <div class="container">
      <div class="row">
      <div class="col-size-2">
        <h1><?php echo $post->post_title; ?></h1>
        <div class="content-first">
            <?php 
            $content = trim(stripslashes($post->post_content));
            $html = apply_filters("the_content", $content);
            echo $html; ?>
        </div>
		<div class="content-last">
			<?php 
            $content = trim(stripslashes(get_post_meta(get_the_ID(),'product_details',true)));
            $html = apply_filters("the_content", $content);
             echo $html; ?>
         </div>
        <a class="button button-yellow" href="<?php echo get_permalink( 11 ); ?>?cType=<?php echo get_cat_name($category[0]->term_id); ?>&cSize=<?php $post_tag = wp_get_post_tags($post->ID); echo ($post_tag[0]->name); ?>">Get a quote</a>
      </div>
      <div class="col-size-2">
      <div class="product-image">
          <div class="product-image-featured">
          <?php  $img_featured = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' ); ?>
          <img src="<?php echo $img_featured[0]; ?>" alt="<?php echo $post->post_title; ?>"/> 
          </div>
          <div class="product-image-list">
			<?php 
				$images = get_multi_images_src('medium','full',false,get_the_ID());
				
				foreach($images as $img){
					
				?>
                <div class="each">
           	  		<img src="<?php echo $img[1][0]; ?>"/>
              	</div>
				<?php	
				}
			?>
            
            </div>
        </div>
      </div>
    </div>
    </div>
    <!-- .container --> 
  </section>
  <section id="product-specification">
    <section id="product-specification-header">
      <div class="container">
        <h2>Specifications</h2>
      </div>
    </section>
    <div class="container">
      <div class="product-specification-content">
        <div class="row">
          <div class="col-size-2">
            <table id="product">
              <tr>
                <td><span>Type:</span></td>
                <td><?php 		
								$cats = '';
								foreach($category as $cat){
										$cats .= get_cat_name($cat->term_id) . ', ';

								}
								echo trim($cats, ', ');
					 ?></td>
              </tr>
              <tr>
                <td><span>Condition:</span></td>
                <td><?php echo get_post_meta(get_the_ID(),'condition',true); ?></td>
              </tr>
              <tr>
                <td><span>Length:</span><br></td>
                <td><?php $post_tag =  wp_get_post_tags(get_the_ID()); 
							$tag_show = '';
							foreach($post_tag as $tag){
									$tag_show .= $tag->name.', ';
							}
							echo trim($tag_show, ', ');
						?></td>
              </tr>
              <?php if(get_post_meta(get_the_ID(),'volume_content',true)){ ?>
                  <tr>
                    <td><span>Volume content:</span></td>
                    <td><?php echo get_post_meta(get_the_ID(),'volume_content',true); ?>m³</td>
                  </tr>
              <?php } ?>
              <?php if(get_post_meta(get_the_ID(),'max_weight',true)){ ?>
              <tr>
                <td><span>Max. Weight:</span></td>
                <td><?php echo get_post_meta(get_the_ID(),'max_weight',true); ?>kg</td>
              </tr>
			  <?php } ?>
            </table>
          </div>
          <div class="col-size-2">
            <table id="product">
            <?php if(get_post_meta(get_the_ID(),'internal_dimensions',true)){ ?>
              <tr>
                <td><span>Standard <i>[Height x Width]</i>:</span></td>
                <td><?php echo get_post_meta(get_the_ID(),'internal_dimensions',true); ?></td>
              </tr>
              <?php } ?>
              <?php if(get_post_meta(get_the_ID(),'external_dimensions',true)){ ?>
              <tr>
                <td><span>High cube <i>[Height x Width]</i>:</span></td>
                <td><?php echo get_post_meta(get_the_ID(),'external_dimensions',true); ?></td>
              </tr>
              <?php } ?>
              <?php if(get_post_meta(get_the_ID(),'door_opening',true)){ ?>
              <tr>
                <td><span>Door opening:</span></td>
                <td><?php echo get_post_meta(get_the_ID(),'door_opening',true); ?>mm <i>[BxH]</i></td>
              </tr>
              <?php } ?>
              <?php if(get_post_meta(get_the_ID(),'tare',true)){ ?>
              <tr>
                <td><span>Tare:</span></td>
                <td><?php echo get_post_meta(get_the_ID(),'tare',true); ?>kg</td>
              </tr>
              <?php } ?>
              <?php if(get_post_meta(get_the_ID(),'payload',true)){ ?>
              <tr>
                <td><span>Payload:</span></td>
                <td><?php echo get_post_meta(get_the_ID(),'payload',true); ?>kg</td>
              </tr>
              <?php } ?>
            </table>
          </div>
        </div>
        <!-- .container --> 
      </div>
    </div>
  </section>
</article>
<?php get_footer(); ?>
