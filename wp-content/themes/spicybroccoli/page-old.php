<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */

get_header(); ?>
    <section id="page"  role="main">
        <div class="container">
        	<h1><?php echo $post->post_title; ?></h1>
            <?php 
            $content = trim(stripslashes($post->post_content));
            $html = apply_filters("the_content", $content);
            echo $html; ?>
            
        </div><!-- #container -->
    </section><!-- #main -->

<?php get_footer(); ?>