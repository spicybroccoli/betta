<?php

/**

* Theme Functions

*/

/**

* Set the content width based on the theme's design and stylesheet.

* Used to set the width of images and content. Should be equal to the width the theme

* is designed for, generally via the style.css stylesheet.

*/

if ( ! isset( $content_width ) )

$content_width = 640;

/** Tell WordPress to run twentyten_setup() when the 'after_setup_theme' hook is run. */

add_action( 'after_setup_theme', function() {

// This theme uses post thumbnails

add_theme_support( 'post-thumbnails' );

add_theme_support( 'html5', array( 'comment-list', 'comment-form', 'search-form', 'gallery', 'caption' ) );

// This theme uses wp_nav_menu() in one location.

register_nav_menus( array(

'primary' => 'Primary Navigation',

) );

});

add_filter ( 'wp_tag_cloud', 'tag_cloud_font_size_class' );  

function tag_cloud_font_size_class( $taglinks ) {

//$tags = explode('</a>', $taglinks);

$regex1 = "#(.*style='font-size:)(.*)((pt|px|em|pc|%);'.*)#e"; 

$regex2 = "#(style='font-size:)(.*)((pt|px|em|pc|%);')#e";         

$regex3 = "#(.*class=')(.*)(')( title=')(.*)(')(>.*)#e";   

//$regex4 = "#(.<\\/a>)#e";         
if(is_array($taglinks)){
foreach( $taglinks as $tag ) {    

$size = preg_replace($regex1, "(''.round($2).'')", $tag ); //get the rounded font size       

$tag = preg_replace($regex2, "('')", $tag ); //remove the inline font-size style

$tag = preg_replace($regex3, "('$1tag-size-'.($size).' $2$3$7')", $tag ); //add .tag-size-{nr} class

//$tag = preg_replace($regex4, "($1')", $tag ); //add .tag-size-{nr} class

$tagn[] = $tag;

}     
}

//$taglinks = implode('</a>', $tagn);    

$taglinks = $tagn ;

return $taglinks; 

}

// Bootstrap Theme

include('inc/template-utils.php'); // Load Template Utilities

include('inc/branding.php'); // Load Template Utilities

include('inc/responsive-nav.php'); // Responsive Nav

// include('inc/landing.php'); // Load Landing page if necessary

include('inc/scripts.php'); // Load Scripts

//multiples images plugin

add_filter('images_cpt','my_image_cpt');

function my_image_cpt(){

$cpts = array('post');

return $cpts;

}

add_filter('list_images','my_list_images');

function my_list_images(){

//SHOW 6 IMAGES

$picts = array(

'image1' => '_image1',

'image2' => '_image2',

'image3' => '_image3',

'image4' => '_image4',

'image5' => '_image5',

'image6' => '_image6'

);

return $picts;

}

/* limit the content without split a word

* ## DO NOT ## USE the_content() <- Its come with html tags

* --- >> USE get_the_content() INSTEAD

*/

function getLimitedText($content, $size = 200, $dodots = true, $strip = true) {

($strip)?$content = wp_strip_all_tags($content):'';

if(strlen($content) > $size){

$pos = stripos( $content, ' ', $size);

$content = substr( $content, 0, $pos ); 

$dots = ($dodots)?'...':'';

}

return $content . $dots;

}

// CREATE A CONTAINER TYPE LIST MENU

function wpcf7_containerSize_shortcode_handler($tag){

$wptc = get_tags(); // get the tags

$return ='';

$return .= '<select name="container-size" class="wpcf7-form-control wpcf7-select" aria-invalid="false">';

$return .= '<option disabled selected>Container Size?</option>';

foreach( $wptc as $wpt ) {

$return .= '<option value="'.$wpt->name.'"';

if($_GET['cSize'] == $wpt->name){$return .=  ' selected';}	

$return .='>'.$wpt->name.'</option>';

}//*/

$return .= '</select>';

return $return;

}

// CREATE A CONTAINER TYPE LIST MENU

function wpcf7_containerType_shortcode_handler($tag){

$args = array(

'child_of' => 5, //Category of products size

'title_li' => '',

'hide_empty' => 0,

'echo'		=> 0

);

$select = wp_dropdown_categories( $args );

$replace = '<select name="container-type" class="wpcf7-form-control wpcf7-select" aria-invalid="false">';

$replace .= '<option disabled selected>Container Type?</option>';

$return  = preg_replace( '#<select([^>]*)>#', $replace, $select );

$return  = preg_replace( '#>' . $_GET['cType'] . '</option>#', '# selected>' . $_GET['cType'] . '</option>#', $return );

return $return;

}

wpcf7_add_shortcode( 'listcontainertypes', 'wpcf7_containerType_shortcode_handler', true );

wpcf7_add_shortcode( 'listcontainersizes', 'wpcf7_containerSize_shortcode_handler', true );

