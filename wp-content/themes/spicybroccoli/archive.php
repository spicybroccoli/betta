<?php get_header(); ?>

    <section id="page"  role="main">

        <div class="container">



            <h1><?php if($cat != 5){?>Containers - <?php } echo single_tag_title(); ?></h1>

		<p class="category-description"> <?php echo category_description( $cat ); ?> </p>

        <div class="row">

        <ul class="product-list">

        	<?php while ( have_posts() ) : the_post(); ?>

            <li class="col-size-1 col-md-2 col-sm-2 col-xs">

                <?php //$feat_image = wp_get_attachment_url( get_post_thumbnail_id($post->ID, 'medium') ); ?>

                <a href="<?php the_permalink(); ?>" alt="<?php echo $post->post_title; ?>">

                	<?php the_post_thumbnail( 'thumbnail', array( 'class' => 'img-responsive' )  );  ?>

                    </a>

	        	<h2><a href="<?php the_permalink(); ?>" alt="<?php echo $post->post_title; ?>"><?php echo $post->post_title; ?></a></h2>

	            <?php /*<p><?php 

				//ITS IS A CUSTOM FUNCTION (functionS.php)

				echo getLimitedText(get_the_content(), 100); ?></p>*/ ?>

            </li>

            <?php endwhile; ?>

            </ul>

        </div>

        </div><!-- #container -->

    </section><!-- #main -->



<?php get_footer(); ?>