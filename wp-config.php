<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */
 
// Include local configuration
if (file_exists(dirname(__FILE__) . '/local-config.php')) {
	include(dirname(__FILE__) . '/local-config.php');
}

// Global DB config
if (!defined('DB_NAME')) {
	define('DB_NAME', 'db142268_betta');
}
if (!defined('DB_USER')) {
	define('DB_USER', 'db142268');
}
if (!defined('DB_PASSWORD')) {
	define('DB_PASSWORD', 'fingered!');
}
if (!defined('DB_HOST')) {
	define('DB_HOST', 'internal-db.s142268.gridserver.com');
}

/** Database Charset to use in creating database tables. */
if (!defined('DB_CHARSET')) {
	define('DB_CHARSET', 'utf8');
}

/** The Database Collate type. Don't change this if in doubt. */
if (!defined('DB_COLLATE')) {
	define('DB_COLLATE', '');
}

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         ',rtbRnG3m$GEIH(&U$[P4|n.mIyvxzpvlE} u`1lbFRI.Y+`+RMPY<EOuYe{fTaY');
define('SECURE_AUTH_KEY',  'g.E5+dR !0@WP1_;-<sztK5u_la9,+lVXH)|pwby/,W#Es|MM.0=r|Z(I)ds3vJE');
define('LOGGED_IN_KEY',    'YIe:ooupvUMlEszHP58Hdqgxc2&+V&ZR:3*S_~%{]g_KZh=g<0rQ9r< ak V|bJm');
define('NONCE_KEY',        'GrVn0b`rO5vR<=xN!.SmA,ub|>-Be8]$.#omQflpCu23-,(/`q-y0Y#DAq>7kuah');
define('AUTH_SALT',        ']9M0/] P1E}Nn#zeG~;z<Te*Hnm6j;;)kclxmB9m/>>BDkpNa7wh`Y%/rm-#3~eU');
define('SECURE_AUTH_SALT', '&@52-z_|WuNa>k8dxSCN*@-B9M[;X~LF& <>xxf|5bqNwHp<Zr0bfaF jg{|KQ2!');
define('LOGGED_IN_SALT',   'kcAmyzWg /Ub]WvD=&Bl#2F3v$,oA0C5z-#+W*g^+~7@ALFPEt ,)84-Y[YJdu+L');
define('NONCE_SALT',       '3O/<1l_yU[xBV|/Tnb~PIDKcX:!%,/2_(ScellgxjDyMOcL5xD b;&_M:%a@-RAM');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'sbm_';

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress. A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
 * language support.
 */
define('WPLANG', '');



/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
if (!defined('WP_DEBUG')) {
	define('WP_DEBUG', false);
}

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
